const http = require('http');
const express = require('express');
// const bodyParser = require("body-parser");
const app = express();
const port = 3000;
app.use(express.json({limit: '50mb'}));
app.get('/', (req, res) => {
    const message = { message: 'Hello World'};
    res.send(message);
});
http.createServer(app.handle.bind(app)).listen(port,()=>{
    console.log("Listening...on port "+port);
});